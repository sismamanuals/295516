.. |area_di_lavoro_ruler| image:: _static/icons/Ruler.svg
   :height: 1.0 cm
   :align: middle

.. |area_di_lavoro_draw| image:: _static/icons/Edit.svg
   :height: 1.0 cm
   :align: middle

.. |area_di_lavoro_custom_machineRed| image:: _static/icons/custom_machineRed.svg
   :height: 1.0 cm
   :align: middle

.. |area_di_lavoro_custom_open| image:: _static/icons/custom_open.svg
   :height: 1.0 cm
   :align: middle

.. |area_di_lavoro_eye| image:: _static/icons/Eye.svg
   :height: 1.0 cm
   :align: middle

.. |area_di_lavoro_border_color| image:: _static/icons/BorderColor.svg
   :height: 1.0 cm
   :align: middle

.. |area_di_lavoro_fill_color| image:: _static/icons/FillColor.svg
   :height: 1.0 cm
   :align: middle

.. |area_di_lavoro_laser| image:: _static/icons/Laser.svg
   :height: 1.0 cm
   :align: middle

.. |area_di_lavoro_level_order| image:: _static/icons/LevelOrder.svg
   :height: 1.0 cm
   :align: middle

.. _area_di_lavoro:

##############
AREA DI LAVORO
##############
L'area di lavoro è la parte centrale della finestra dell'applicazione. In questo spazio è normalmente visualizzata una griglia millimetrica che rappresenta l'area di lavoro del laser.

Quando viene caricato un progetto, in questo spazio è visualizzata l'anteprima della parte grafica che si intende realizzare. L'immagine è funzionale e permette di capire gli ingombri e le dimensioni della lavorazione. In questa rappresentazione non sono visualizzati, per la comprensione del disegno, i reali tratti laser. 

Nello stesso spazio possono comparire delle anteprime 3D per facilitare la centratura ed il posizionamento di lavorazioni sulle analoghe superfici reali.

.. _area_di_lavoro_proprietà_grafiche:

******************
PROPRIETÀ GRAFICHE
******************
È possibile, per ogni elemento posizionato sul piano di lavoro, esplicitare ulteriormente alcuni parametri.

Cliccando due volte sull'elemento desiderato si accede ad una nuova finestra, visibile aprendo la toolbar di editing.

In base alla tipologia di oggetto sono possibili 3 diverse interfacce di editing:

* interfaccia per elementi composti da linea aperta (:numref:`fig_toolbar_draw_impostazioni_linea_aperta`);
* interfaccia per elementi composti da linea chiusa (:numref:`fig_toolbar_draw_impostazioni_linea_chiusa`);
* interfaccia per elementi composti da linea chiusa e pieni (:numref:`fig_toolbar_draw_impostazioni_linea_chiusa_piena`).

.. _fig_toolbar_draw_impostazioni_linea_aperta:
.. figure:: _static/icons/icons_border.png 
   :width: 14 cm
   :align: center

   :guilabel:`Barra strumenti` - :guilabel:`Draw` - Linea aperta

.. _fig_toolbar_draw_impostazioni_linea_chiusa:
.. figure:: _static/icons/icons_closed_border.png
   :width: 14 cm
   :align: center

   :guilabel:`Barra strumenti` - :guilabel:`Draw` - Linea chiusa

.. _fig_toolbar_draw_impostazioni_linea_chiusa_piena:
.. figure:: _static/icons/icons_fill.png
   :width: 14 cm
   :align: center

   :guilabel:`Barra strumenti` - :guilabel:`Draw` - Linea chiusa piena

In queste interfacce sono presenti, in base al tipo di elemento, le seguenti impostazioni:

* :guilabel:`Border`: permette di modificare il bordo del layer dell'elemento selezionato;
* :guilabel:`Fill`: permette di modificare il riempimento del layer dell'elemento selezionato;
* :guilabel:`Z Pos.`: permette di modificare l'altezza della testa laser di lavorazione;
* :guilabel:`Texture path`: permette di definire il percorso di un'immagine da inserire sul piano di lavoro per poterla marcare;
* :guilabel:`Group`: segnala che l'insieme degli elementi è un gruppo;
* :guilabel:`Width`, :guilabel:`Height`, :guilabel:`Radius`: permettono di modificare le dimensioni dell'elemento (in funzione della sua forma).

.. _area_di_lavoro_menù_contestuale_area_di_lavoro:

Menù contestuale area di lavoro
===============================
Cliccando con il tasto destro del mouse nell'area di lavoro si apre il menù in :numref:`fig_toolbar_area_di_lavoro_menu_contestuale_arealavoro`.

.. _fig_toolbar_area_di_lavoro_menu_contestuale_arealavoro:
.. figure:: _static/icons/menu_contestuale_arealavoro.png 
   :width: 8 cm
   :align: center

   Menù contestuale area di lavoro

Dal menù è possibile :

* |area_di_lavoro_custom_open| :guilabel:`Open file`: aprire un file;
* |area_di_lavoro_draw| :guilabel:`Draw`: disegnare;
* |area_di_lavoro_custom_machineRed| :guilabel:`Red`: proiettare il rosso laser;
* |area_di_lavoro_ruler| :guilabel:`Guide lines`: inserire le linea guida.

.. _area_di_lavoro_menù_contestuale_elemento_grafico:

Menù contestuale elemento grafico
=================================
Cliccando con il tasto destro del mouse su un elemento grafico del piano di lavoro si apre il menù in :numref:`fig_toolbar_draw_menù_veloce`.
Nel caso si prema sul piano di lavoro senza la presenza di un elemento grafico si apre il menù descritto alla :numref:`area_di_lavoro_menù_contestuale_area_di_lavoro`.

.. _fig_toolbar_draw_menù_veloce:
.. figure:: _static/toolbar_draw_menù_veloce.png
   :width: 10 cm
   :align: center

   Menù con comandi rapidi.

1. |area_di_lavoro_eye|: rende visibile o nasconde l'oggetto sul quale si è aperto il menù;
2. |area_di_lavoro_border_color|: cambia il colore del livello (bordo) con i parametri di marcatura al quale è riferito l'oggetto (sia bordo che interno). Premendo sull'icona si apre un sotto menù contenente i vari livelli (:numref:`fig_toolbar_draw_menù_veloce_border_color`);

.. _fig_toolbar_draw_menù_veloce_border_color:
.. figure:: _static/toolbar_draw_menù_veloce_border_color.png
   :width: 10 cm
   :align: center

   Menù veloce - Cambio colore bordo del livello

3. |area_di_lavoro_fill_color|: cambia il colore del livello (interno) con i parametri di marcatura al quale è riferito l'oggetto (sia bordo che interno). Premendo sull'icona si apre un sotto menù contenente i vari livelli (:numref:`fig_toolbar_draw_menù_veloce_fill_color`);

.. _fig_toolbar_draw_menù_veloce_fill_color:
.. figure:: _static/toolbar_draw_menù_veloce_fill_color.png
   :width: 10 cm
   :align: center

   Menù veloce - Cambio colore interno del livello

4. |area_di_lavoro_custom_machineRed|: attiva il *Rosso* del singolo elemento;
5. |area_di_lavoro_laser|: inizia la marcatura del singolo elemento selezionato;
6. |area_di_lavoro_level_order|: modifica il livello dell'elemento selezionato. Premendo si apre un sotto menù nel quale scegliere l'azione da svolgere (:numref:`fig_toolbar_draw_menù_veloce_level_order`).

.. _fig_toolbar_draw_menù_veloce_level_order:
.. figure:: _static/toolbar_draw_menù_veloce_level_order.png
   :width: 10 cm
   :align: center

   Menù veloce - Ordine dei livelli

Comandi rapidi
==============
* :kbd:`Ctrl` + :kbd:`C`: copia l'oggetto o il testo selezionato;
* :kbd:`Ctrl` + :kbd:`V`: incolla l'oggetto o il testo copiato;
* :kbd:`Ctrl` + :kbd:`Z`: annulla l'ultima modifica;
* :kbd:`Ctrl` + :kbd:`Y`: ripristina l'ultima modifica annullata;
* :kbd:`Ctrl` + :kbd:`X`: copia e cancella il testo selezionato (funzione abilitata solo per i testi);
* :kbd:`Ctrl` + :kbd:`A`: seleziona tutto il testo (funzione abilitata solo per i testi);
* :kbd:`Ctrl` + :kbd:`H`: "Homing". Riporta il piano di lavoro al centro della schermata;
* :kbd:`Ctrl` + :kbd:`R`: "Homing Mandrel". Riporta il mandrino all'angolo 0;
* :kbd:`Ctrl` + :kbd:`M`: specchia un oggetto selezionato secondo l'asse Y;
* :kbd:`Ctrl` + :kbd:`G`: crea delle *GuideLine*. Con il primo click si decide l'origine della linea, con il secondo il verso (orizzontale o verticale);
* :kbd:`S`: funzione "Scala". Modifica manuale delle misure di un oggetto selezionato;
* :kbd:`R`: rotazione libera nel piano di un oggetto selezionato.
* :kbd:`M`: funzione "movimenti" dell'oggetto selezionato.  
* :kbd:`M` + :kbd:`X quota desiderata`: abilita spostamento relativo dell'oggetto selezionato lungo l'asse "X"
* :kbd:`M` + :kbd:`Y quota desiderata`: abilita spostamento relativo dell'oggetto selezionato lungo l'asse "Y"
* :kbd:`M` + :kbd:`rel quota 1  quota 2`: abilita spostamento relativo dell'oggetto selezionato lungo i due assi "X" e "Y" (dare uno spazio tra prima e seconda coordinata)