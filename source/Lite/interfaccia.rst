.. include:: lite_interface_images.txt

.. _interfaccia_lite:

################
INTERFACCIA LITE
################
**********************
SCOPO DELL'INTERFACCIA
**********************
Questa interfaccia rappresenta una semplificazione dell'interfaccia completa di SLC³ con funzionalità ridotte per un operatore generico. La semplificazione rende più agevole l'utilizzo del sistema laser.

*****************************
CREAZIONE PROFILO INTERFACCIA
*****************************
Dopo l'avvio del programma SLC³ creare un profilo in :guilabel:`Application Settings`

.. _fig_A:
.. figure:: _static/A.png
   :width: 14 cm
   :align: center

Creare un nuovo profilo per la gestione del software descritto premendo il comando :kbd:`+` e seguendo la procedura guidata.

.. _fig_B:
.. figure:: _static/B.png
   :width: 14 cm
   :align: center

.. _fig_C:
.. figure:: _static/C.png
   :width: 14 cm
   :align: center

Ricordare di spuntare le voci evidenziate in rosso. Al termine premere la spunta per conferma.

********************
ACCESSO AL PROGRAMMA
********************
Registrato il profilo "Lite" è possibile accedervi all'avvio premendo la voce :guilabel:`Users` e impostando le credenziali utilizzate per creare l'account.

Si aprirà la schermata principale dedicata all'impostazione principale di qualsiasi lavoro di marcatura.

.. _fig_0:
.. figure:: _static/0.png
   :width: 14 cm
   :align: center

.. _fig_1:
.. figure:: _static/1.png
   :width: 14 cm
   :align: center

**********************
CREAZIONE DEL PROGETTO   
**********************
Scegliere se iniziare con un nuovo progetto di marcatura o aprirne uno precedentemente creato.

.. _fig_2:
.. figure:: _static/2.png
   :width: 14 cm
   :align: center

Nel caso si dovesse creare un nuovo progetto seguire la procedura guidata e procedere con la configurazione del progetto di lavoro impostando, sulla base delle proprie necessità, la modalità di marcatura più idonea. 

.. _fig_3:
.. figure:: _static/3.png
   :width: 14 cm
   :align: center

* |Lite_interface_images_ring_mode| :guilabel:`Ring mode`: È la modalità di marcatura su anelli. Permette di eseguire incisioni su superfici cilindriche, quali anelli tubi ecc.
* |Lite_interface_images_custom_planar_mode| :guilabel:`Planar mode`: È la modalità di marcatura su piano. Permette di eseguire un ampio numero di marcature su una superficie piana.

Al termine inserire l'altezza del pezzo che si desidera marcare nell'apposito spazio :guilabel:`Z position` (altezze espresse in mm)

.. _fig_planare:
.. figure:: _static/planare.png
   :width: 14 cm
   :align: center

Proseguire scegliendo il materiale che si desidera marcare.

.. _fig_planare-2:
.. figure:: _static/planare-2.png
   :width: 14 cm
   :align: center

Una volta selezionato, scegliere tra le varie voci l'elemento grafico da marcare nel progetto di lavoro.

.. _fig_planare-3:
.. figure:: _static/planare-3.png
   :width: 14 cm
   :align: center

Selezionare le dimensioni dell'oggetto grafico desiderato.

.. _fig_planare-4:
.. figure:: _static/planare-4.png
   :width: 14 cm
   :align: center

Completata la seguente procedura selezionare il processo laser da voler applicare al proprio progetto di lavoro.

.. _fig_planare-5:
.. figure:: _static/planare-5.png
   :width: 14 cm
   :align: center

Al termine premere :guilabel:`conferma` per confermare le impostazioni precedenti.

Eseguire l'homing della macchina. Al temine dell'operazione verrà visualizzata l'interfaccia con il progetto di marcatura configurato.

.. _fig_planare-mode:
.. figure:: _static/planare-mode.png
   :width: 14 cm
   :align: center

********************
INTERFACCIA GENERALE
********************
L'ambiente di progettazione di SLC³ Lite si compone di un'interfaccia grafica composta da:

* :guilabel:`Wizard` (:numref:`wizard`)
* :guilabel:`Barra degli strumenti` (:numref:`tools_bar`)
* :guilabel:`Area di lavoro` (:numref:`working_area`)
* :guilabel:`Finestra laterali` (:numref:`lateral_window`)
* :guilabel:`Barra di stato` (:numref:`status_bar`)

.. _fig_interface:
.. figure:: _static/interface.png
   :width: 14 cm
   :align: center

.. _wizard:

******
WIZARD
******
Rappresenta la finestra principale di configurazione dei progetti di marcatura. Seguendo la procedura guidata è possibile 
importare o creare progetti di marcatura a seconda delle proprie esigenze. 

.. _fig_wizard:
.. figure:: _static/wizard.png
   :width: 14 cm
   :align: center

All'interno della precedente finestra è inoltre possibile usufruire di una barra di suggerimenti per facilitare l'operatore nell'utilizzo dei vari comandi della macchina.

.. _fig_suggestion:
.. figure:: _static/suggestion.png
   :width: 14 cm
   :align: center

.. _tools_bar:

*********************
BARRA DEGLI STRUMENTI
*********************
Rappresenta la barra dei comandi principali consentiti dal programma. Sulla base degli optional della macchina poi è configurato di conseguenza il software.
Lo standard racchiude i seguenti comandi:

* |Lite_interface_images_custom_start| :kbd:`Start`
* |Lite_interface_images_custom_stop| :kbd:`Stop`

.. _working_area:

***********
AREA LAVORO
***********
Rappresenta l'area su cui è possibile eseguire lavorazioni. Varia a seconda della focale montata.

Quello che non rientra all'interno dell'area non è lavorabile dalla macchina.

.. _fig_working:
.. figure:: _static/working.png
   :width: 14 cm
   :align: center

.. _lateral_window:

*****************
FINESTRA LATERALE
*****************
È suddivisa in due parti una dedicata alle proprietà grafiche dei file importati.

.. _fig_file:
.. figure:: _static/file.png
   :width: 6 cm
   :align: center

L'altra è dedicata ai processi di marcatura.

.. _fig_process:
.. figure:: _static/process.png
   :width: 6 cm
   :align: center

.. _status_bar:

**************
BARRA DI STATO
**************
È suddivisa in: 

* |Lite_interface_images_custom_Waiting_jobs| :kbd:`Waiting jobs`
* |Lite_interface_images_custom_Process_time| :kbd:`Process time`
* |Lite_interface_images_custom_Laser_time| :kbd:`Laser time`
* |Lite_interface_images_custom_status| :kbd:`Status`
* |Lite_interface_images_custom_messages| :kbd:`Messages`
