.. include:: titlebar_images.txt

.. _barra_del_titolo:

################
BARRA DEL TITOLO
################
La barra del titolo è la porzione superiore di finestra. Questo spazio viene dedicato al nome del dell'applicazione. Sono presenti, inoltre, alcuni comandi rapidi per l'accesso a funzioni comuni:

* |titlebar_custom_file_new| :kbd:`Nuovo progetto`: crea un nuovo progetto
* |titlebar_custom_open| :kbd:`Apri progetto`: accede al file system e permette la ricerca di un progetto esistente
* |titlebar_custom_save| :kbd:`Salva progetto`: salva il progetto corrente
* |titlebar_custom_saveas| :kbd:`Salva progetto con nome`: salva il progetto corrente chiedendo di assegnare un nuovo nome
* |titlebar_custom_undo| :kbd:`Torna indietro`: annulla l'ultima operazione eseguita
* |titlebar_custom_redo| :kbd:`Ripeti operazione annullata`: ripristina l'ultima operazione annullata
* |titlebar_custom_preview| :kbd:`Anteprima marcatura`: apre la finestra di anteprima della marcatura
* |titlebar_custom_stop| :kbd:`Stop`: blocca l'operazione in corso fermando laser, automazione e sistema di visione

Sul lato opposto sono presenti i comandi dedicati alla configurazione:

* |titlebar_icon_user| :kbd:`User`: accedere all'interfaccia utenti
* |titlebar_icon_settings| :kbd:`Impostazioni`: accede alla configurazione dell'applicazione e della macchina (:numref:`impostazioni_generali`)
* |titlebar_icon_services| :kbd:`Servizi`: accede alla gestione dei servizi
* |titlebar_information_outline| :kbd:`Informazioni su..`: visualizza la versione dell'applicazione e dei suoi componenti

Per completare la sequenza di comandi sono presenti le normali funzioni di gestione della finestra dell'applicazione.

.. _impostazioni_generali:

*********************
IMPOSTAZIONI GENERALI
*********************
Premendo sul pulsante :kbd:`Settings` si apre la finestra in :numref:`fig_impostazioni_generali_globale` nella quale è possibile modificare alcune impostazioni di SLC³ come, ad esempio, le impostazioni globali del software.

:guilabel:`Global`
==================

.. _fig_impostazioni_generali_globale:
.. figure:: _static/impostazioni_generali_globale.png
   :width: 14 cm
   :align: center

   Impostazioni generali - :guilabel:`Global`

:guilabel:`Language`
   * Menù per modificare la lingua utilizzata all'interno del programma.

:guilabel:`Layout`
   * :kbd:`Disable layout saving`: disabilita il salvataggio dei layout creati durante una lavorazione.

:guilabel:`Options`
   * :kbd:`Save project after work`: salva un progetto eseguito al termine della marcatura;
   * :kbd:`Sort marking by color`: esegue le lavorazioni secondo colore completando tutte le colonne che compongono un layer anziché terminare la prima colonna di un layer e passare a quello successivo per poter completare la sua prima colonna proseguendo poi con le successive;
   * :kbd:`Save textures`: salva le texture all'interno di ogni specifico progetto;

   .. TIP::
      |tip| l'utilizzo di questa impostazione aumenta la dimensione dei progetti creati e lo spazio da loro occupato su disco.

   * :kbd:`Close protections on start`: chiude le porte di protezione alla pressione del pulsante :kbd:`Start`;
   * :kbd:`Filter 3D data`: filtro dati per alleggerire file grafici 3D. Si consiglia il suo utilizzo in caso di lavorazioni con mandrino su testa Standard (Non DFS) per migliorare la qualità di incisione e ridurre i tempi di ciclo;

   .. TIP::
      |tip| rimuovere la spunta in caso di lavorazioni 3D con Dynamic Focus Shifter (DFS).

   * :kbd:`Multiple laser params`: permette di utilizzare più parametri differenti all'interno di ogni Laser Job.
  
:guilabel:`Job Report Setting`
   * :kbd:`Enable job report`: crea un report durante la lavorazione. Si può controllare lo stato del laser e le informazioni che vengono inviate in caso di errori.

:guilabel:`Import / Export Settings`
   * :kbd:`Import`: importa impostazioni generali;
   * :kbd:`Export`: esporta impostazioni generali.

:guilabel:`Users`
=================
:guilabel:`Add user`
   * :kbd:`User pro`: profilo dedicato a utente esperto. Permette la gestione e la modifica delle interfacce :guilabel:`User basic`; 
   * :kbd:`User basic`: profilo utente :guilabel:`User basic`, con funzioni macchina ridotte.

:guilabel:`Machine`
===================
La configurazione all'interno della cartella varia in base alla macchina, alla sorgente ed alla testa montata. 

:guilabel:`Tools`
=================
La configurazione varia in base agli accessori installati.
