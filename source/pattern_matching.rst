.. include:: toolbar_pattern_matching.txt

.. _barra_strumenti_pattern_matching:

################
PATTERN MATCHING
################
La funzione di creazione del modello è disponibile solamente se la chiavetta USB Pattern Matching è inserita nel sistema. Nella finestra :guilabel:`Pattern Matching` (:numref:`fig_pattern_matching_ribbon_bar`) l'utente può trovare due icone per la gestione del *pattern matching*:

* |toolbar_pattern_matching_icon_ModelCreation| Creazione modello;
* |toolbar_pattern_matching_database| Libreria modelli.

.. _fig_pattern_matching_ribbon_bar:
.. figure:: _static/PM_ribbon_bar.png
   :width: 14 cm
   :align: center

   Pagina :guilabel:`Pattern Matching`

.. _barra_strumenti_pattern_matching_models_creation_scan:

******************************
CREAZIONE MODELLI DA SCANSIONE
******************************
Con questa funzione l'utente può creare i modelli che verranno utilizzati nelle procedure di abbinamento (per esempio Posizionamento e
Centraggio dell'anello). Una volta creato il modello sarà disponibile nella libreria dei modelli. Ci sono quattro passaggi per creare un modello:

1. **Passaggio 1**

   * **Load image, model dxf**: (es. un'immagine scansionata) o (carica la creazione di un modello dxf dalla sezione DXF);

   * **Scan the scene**: prima di eseguire l'operazione di scansione, è utile impostare il tempo di esposizione e l'offset Z. Il parametro Z offset specifica il valore Z utilizzato per la scansione relativa alla posizione di Z, specificato come "Z iniziale". Facendo clic sul pulsante di attivazione / disattivazione è possibile visualizzare la telecamera dal vivo e impostare l'offset Z utilizzando il pulsante **JOG** degli assi o una casella di testo.

   Esistono due modi per eseguire la scansione della scena:
   
   1. scansione dell'area; *oppure*
   2. scansione dell'anello.

   Il primo è sempre disponibile, il secondo è disponibile solo se l'utente ha creato un oggetto Anello 3D e si trova all'interno dell'area di lavoro corrispondente.

   .. _fig_pattern_matching_1:
   .. figure:: _static/PM_1.png
      :width: 14 cm
      :align: center

   Creazione modello - Step 1

   .. _fig_pattern_matching_2:
   .. figure:: _static/PM_2.png
      :width: 14 cm
      :align: center

      Scansione dell'area e Scansione dell'anello

   **AREA SCAN**
      Nella sezione :guilabel:`Area` cliccando sul pulsante :kbd:`Scan` viene eseguita una scansione planare dell'area specificata. Altrimenti è possibile caricare un'immagine da un file o modificare un modello esistente.

   **RING SCAN**
      Nella sezione :guilabel:`Ring` cliccando sul pulsante :kbd:`Scan` viene eseguita una scansione dell'anello utilizzando i parametri specificati. L'utente può eseguire una scansione esterna / interna o verticale cliccando sull'icona preferita.

   .. _fig_pattern_matching_3:
   .. figure:: _static/PM_3.png
      :width: 14 cm
      :align: center

   Al termine della scansione, il passaggio successivo consiste nel selezionare l'area di interesse sull'immagine e l'area del filtro in pixel.

   .. TIP::
      |tip| Il filtro mediano opera su finestre quadrate con il valore del bordo specificato nel campo :guilabel:`median filter [px]`. Si consiglia di utilizzare valori più bassi per immagini piccole e valori più alti per immagini più grandi.

2. **Passaggio 2**

   * **Set regions of interest**: il secondo passo consiste nella selezione delle regioni di interesse:

      * Cliccando sul pulsante :kbd:`Additive` l'utente può aggiungere le regioni usate per estrarre le caratteristiche usate per la corrispondenza (*matching*);
      * Cliccando sul pulsante :kbd:`Subtractive` l'utente può aggiungere le regioni che non verranno utilizzate per la corrispondenza (*matching*).

   .. _fig_pattern_matching_4:
   .. figure:: _static/PM_4.png
      :width: 14 cm
      :align: center

      Creazione modello - Step 2

   :kbd:`Elaborate Regions` esegue l'analisi dell'immagine e, dopo pochi secondi, l'utente può passare alla fase successiva e può vedere le caratteristiche estratte (in rosso) sull'immagine.

3. **Passaggio 3**

   * **Set detection parameters**: nella terza fase l'utente può variale la lunghezza minima dei bordi e il contrasto e vedere i risultati sull'immagine.

   Una volta che l'utente ha selezionato dei buoni parametri può procedere alla fase finale.

   .. _fig_pattern_matching_5:
   .. figure:: _static/PM_5.png
      :width: 14 cm
      :align: center

      Creazione modello - Step 3

   Dopo essere entrati in modalità Modifica, nella pagina :guilabel:`Vision System` verrà mostrata la nuova interfaccia.

4. **Passaggio 4**

   * **Finalize**: nella fase finale l'utente può scegliere gli ultimi parametri per la creazione del modello:

      * :guilabel:`Start Angle` e :guilabel:`Angle Range`: determinano la gamma di possibili rotazioni in cui il modello può trovarsi. Se l'utente desidera cercare modelli in un intervallo di +-20°, lo :guilabel:`Start Angle` deve essere impostato a -20° e :guilabel:`Angle Range` a 40°;
      * :guilabel:`Min Scale` e :guilabel:`Max Scale`: determinano la gamma di scale possibili (dimensioni) del modello. Una scala del 100% corrisponde alla dimensione originale del modello.

   È possibile utilizzare un unico modello per trovare istanze di dimensioni diverse rispetto ad esso.

   .. _fig_pattern_matching_6:
   .. figure:: _static/PM_6.png
      :width: 14 cm
      :align: center

      Creazione modello - Step 4

   Altri parametri utilizzati per la corrispondenza (*matching*) sono:

   * :guilabel:`Score`: determina quale punteggio una potenziale corrispondenza deve almeno avere per essere considerata come un'istanza del modello nell'immagine. Più elevato viene impostato :guilabel:`Score` più veloce è la ricerca. Se ci si può aspettare che il modello non venga mai occluso nelle immagini allora :guilabel:`Min Score` può essere impostato a 0.8 o addirittura a 0.9;

   * :guilabel:`Num of Objects`: determina il numero di istanze da trovare nell'immagine. Ci sono tre opzioni:

      1. opzione :kbd:`<=`: se nell'immagine si trovano più istanze di :guilabel:`Num of Objects` con un punteggio maggiore di :guilabel:`Min Score` la procedura restituisce un errore. La procedura restituisce un valore valido se viene trovato un valore inferiore o uguale a :guilabel:`Num of Objects`, quel numero viene restituito;
      2. opzione :kbd:`=`: vengono restituite solo le istanze pari a :guilabel:`Num of Objects`. Se vengono trovate meno o più istanze di :guilabel:`Num of Objects` viene restituito un errore;
      3. opzione :kbd:`>`: la procedura restituisce un valore valido solo se vengono trovate più di :guilabel:`Num of Objects` istanze;

   * :guilabel:`Timeout`: se la procedura di corrispondenza (*matching*) raggiunge questo :guilabel:`Timeout` termina senza risultati e restituisce un errore. A seconda dell'intervallo di scala specificato da :guilabel:`Min Scale` e :guilabel:`Max Scale`, la procedura richiede una notevole quantità di tempo;
   * :guilabel:`Median Filter [px]`: è il bordo del filtro mediano che verrà utilizzato per filtrare l'immagine prima di eseguire la procedura di corrispondenza (*matching*).

   Il pulsante :kbd:`Check` crea il modello e verifica la sua correttezza, il pulsante :kbd:`Save` permette di salvarlo su disco. La cartella predefinita è "C:\\Sisma\\PM\\SLC3Models".

.. _barra_strumenti_pattern_matching_models_creation_DXF:

************************
CREAZIONE MODELLO DA DXF
************************
È possibile creare un modello da file DXF. La procedura può leggere il contenuto di un file DXF con la versione DXF versione AC1009, AutoCAD Release 12. Non c'è supporto per altre versioni.

Nella pagina :guilabel:`Import DXF model` è importante selezionare la risoluzione del file (quanti pixel per mm). Cliccando sul pulsante :kbd:`Load DXF` l'utente può selezionare il file da importare.

.. _fig_pattern_matching_7:
.. figure:: _static/PM_7.png
   :width: 14 cm
   :align: center

   Creazione modello - :kbd:`Load DXF`

Se il file è valido, verrà creata un'immagine con contorni bianchi su sfondo nero (:numref:`fig_pattern_matching_8`) e l'utente verrà reindirizzato allo Step 4 della :numref:`barra_strumenti_pattern_matching_models_creation_scan`. In quella sezione l'utente può impostare i parametri di ricerca e creare il modello.

.. _fig_pattern_matching_8:
.. figure:: _static/PM_8.png
   :width: 14 cm
   :align: center

   Creazione modello - Loaded DXF

.. _barra_strumenti_pattern_matching_models_library:

****************
LIBRERIA MODELLI
****************
La libreria mostra i modelli salvati nel sistema.

.. _fig_pattern_matching_9:
.. figure:: _static/PM_9.png
   :width: 14 cm
   :align: center

   :guilabel:`PATTERN MATCHING LIBRARY`

Quando l'utente fa clic su un modello può vedere e modificare i parametri corrispondenti. La gamma dei parametri di scala e dei parametri degli angoli è limitata ai valori corrispondenti specificati nella creazione del modello.
