.. _installazione:

#############
INSTALLAZIONE
#############
********************
REQUISITI DI SISTEMA
********************
SLC³ è stato sviluppato in ambiente Microsoft Visual Studio ed è pienamente compatibile con i sistemi operativi Windows.
Per funzionare è richiesto che nel computer sia installato il componente **Microsoft Framework 4.7.2**, scaricabile gratuitamente dal sito Microsoft.
I requisiti necessari per il funzionamento della macchina sono:

* Sistema Operativo Microsoft Windows 10
* RAM 4 GB versione base, 8 GB per funzionalità avanzate tipo sistema di visione
* Connessioni Ethernet/USB/Seriali a seconda del modello di macchina

Tutti i driver e le librerie necessarie per il corretto funzionamento sono tipicamente legate all'hardware per cui possono differire tra una macchina e l'altra.
Per altri eventuali requisiti e limitazioni si rimanda alle schede tecniche dei prodotti installati.

**************************
PROCEDURA DI INSTALLAZIONE
**************************
L'installazione viene eseguita tramite l'apposito eseguibile **SLC3Setup_X.X.XXXX.XXXXX.exe**. Durante la procedura di installazione vengono eseguiti tutti i controlli per valutare se il computer che si sta utilizzando è compatibile.

Per procedere con l'installazione:

.. _fig_SLC3_install_01_starting:
.. figure:: _static/SLC3_install_01_starting.png
   :width: 8 cm
   :align: center

   Selezione lingua installazione

* Selezionare la lingua per il software di installazione.
* Procedere con :kbd:`Ok`.

.. _fig_SLC3_install_02_starting:
.. figure:: _static/SLC3_install_02_starting.png
   :width: 14 cm
   :align: center

   Avvio installazione

* Procedere con :kbd:`Avanti`.

.. _fig_SLC3_install_03_license:
.. figure:: _static/SLC3_install_03_license.png
   :width: 14 cm
   :align: center

   Contratto di licenza

* Selezionare :kbd:`Accetto i termini del contratto di licenza`.
* Confermare con :kbd:`Avanti`. 

.. _fig_SLC3_install_04_components:
.. figure:: _static/SLC3_install_04_components.png
   :width: 14 cm
   :align: center

   Installazione componenti

* Selezionare dall'elenco i componenti che si desidera installare.
* Confermare con :kbd:`Avanti`. 

.. _fig_SLC3_install_05_path:
.. figure:: _static/SLC3_install_05_path.png
   :width: 14 cm
   :align: center

   Scelta cartella di installazione

* Selezionare la cartella di destinazione per SLC³. Si può mantenere quella già proposta oppure, premendo :kbd:`Sfoglia...`, si può scegliere il percorso di destinazione.
* Confermare con :kbd:`Avanti`. 

.. _fig_SLC3_install_06_desktopIcon:
.. figure:: _static/SLC3_install_06_desktopIcon.png
   :width: 14 cm
   :align: center

   Creare collegamento sul desktop

* Selezionare se creare o meno un'icona di collegamento all'eseguibile di SLC³ sul desktop con :kbd:`Crea un'icona sul desktop`.
* Confermare con :kbd:`Avanti`.

.. _fig_SLC3_install_07_confirm:
.. figure:: _static/SLC3_install_07_confirm.png
   :width: 14 cm
   :align: center

   Avvio installazione

* Confermare con :kbd:`Installa` oppure premere :kbd:`Indietro` per rivedere le scelte.

.. _fig_SLC3_install_08_installing:
.. figure:: _static/SLC3_install_08_installing.png
   :width: 14 cm
   :align: center

   Processo di installazione in corso

* Durante il processo di installazione si deve attendere che i diversi componenti vengano caricati.

In particolare viene chiesta la conferma per l'installazione di una libreria esterna chiamata SlimDX. Se già presente si può annullarne l'installazione.

.. _fig_installazione_slimDX_librerie:
.. figure:: _static/installazione_slimDX_librerie.png
   :width: 14 cm
   :align: center

   Installazione librerie slimDX

* Confermare con :kbd:`Next` per installare le librerie. Se già presenti premere :kbd:`Cancel` e poi, nella finestra successiva, premere :kbd:`Yes`.

.. _fig_installazione_slimDX_librerie_tipo:
.. figure:: _static/installazione_slimDX_librerie_tipo.png
   :width: 14 cm
   :align: center

   Tipo di installazione librerie slimDX

* Selezionare :kbd:`Repair`.

.. _fig_installazione_slimDX_librerie_ripara:
.. figure:: _static/installazione_slimDX_librerie_ripara.png
   :width: 14 cm
   :align: center

   Tipo di installazione librerie slimDX

* Confermare con :kbd:`Repair` oppure premere :kbd:`Back` per rivedere le scelte.

.. _fig_installazione_slimDX_librerie_termine:
.. figure:: _static/installazione_slimDX_librerie_termine.png
   :width: 14 cm
   :align: center

   Termine installazione librerie slimDX

* Premere :kbd:`Finish` per terminare l'installazione di SlimDX Runtime.NET 4.0.

.. _fig_SLC3_install_09_completed:
.. figure:: _static/SLC3_install_09_completed.png
   :width: 14 cm
   :align: center

   Termine installazione SLC³

* Selezionare :kbd:`Avvia SLC3` per avviare l'eseguibile al termine dell'installazione;
* Premere :kbd:`Fine` per concludere.
