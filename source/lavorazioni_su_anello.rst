.. _lavorazioni_su_anello:

#####################
LAVORAZIONI SU ANELLO 
#####################

Per eseguire una lavorazione su anello bisogna:

#. Creare uno sviluppo 3D andando nella sezione :guilabel:`Disegno -> Forme 3D` e selezionare :kbd:`RING`;
#. Inserire i parametri richiesti:

   * **Diametro esterno [mm]**
   * **Diametro interno [mm]**
   * **Lunghezza [mm]**

#. Premere il pulsante :kbd:`OK`;
#. Sul piano di lavoro verrà visualizzato l'anello con visuale dall'alto.

Le visualizzazioni si compongono di 2 finestre:

   * La finestra di sinistra: rappresenta lo sviluppo dell'anello in piano 2D. Lo spazio tra le 2 linee blu della zona 2 rappresenta la superficie del diametro interno dell'anello e lo spazio tra le linee guida rosse della zona 1 rappresenta la superficie del diametro esterno. Nella parte centrale viene riportato la visualizzazione del profilo frontale esterno posto perpendicolarmente alle precedenti due preview;
   * La finestra di destra: rappresenta la visualizzazione 3D. Permette di vedere sull'anello la rappresentazione degli elementi che inseriamo sui piani di lavoro della finestra di sinistra. 

.. _fig_ring:
.. figure:: _static/ring.png
   :width: 20 cm
   :align: center

Le linee guida sono:

#. Orizzontali:

   * Linea **BLU**: linea inferiore che rappresenta la linea di fuoco pezzo. Modificando la sua posizione, alzandola od abbassandola all'interno della fascia grigia che rappresenta l'anello, si varia il fuoco del laser rispettivamente nella parte più bassa o più alta del pezzo fisico in questione;
   * Linea **ROSSA**: linea superiore che rappresenta il centro dell'anello.

#. Verticali:

   * Linee **ROSSE**: rappresentano i margini più esterni entro i quali è possibile eseguire una lavorazione (fare riferimento alla parte superiore);
   * Linee **BLU**: rappresentano l'intervallo nel quale è possibile eseguire una lavorazione interna anello (fare riferimento alla parte inferiore).

.. _modifica_grafica_e_progettazione:

********************************
MODIFICA GRAFICA E PROGETTAZIONE
********************************
Per modificare lo sviluppo dell'anello:

* Premere due volte col tasto sinistro sull'oggetto per aprire la finestra :guilabel:`Graphic Properties`;
* Premere sul pulsante :kbd:`Draw`.

Per aggiungere degli elementi da marcare sull'anello:

* Utilizzare i normali comandi della sezione :kbd:`Disegna`; *oppure*
* Collegare un file SVG creato in precedenza:

   * andare in :guilabel:`Working Plane` (vedere :numref:`finestre_laterali`;
   * premere il pulsante che permette di aggiungere delle azioni da eseguire nella fase pre o post marcatura;
   * scegliere la voce :kbd:`Link files on ring`;
   * premere sul pulsante :kbd:`Add`;
   * premere per aprire il menù dei parametri che sarà uguale a quello nella figura seguente (fatta eccezione per il parametro di ricerca file in quanto prima si deve premere su :kbd:`Add File` e lo si può fare più volte);

* Scegliere un file da collegare e, per visualizzarlo sul piano di lavoro, premere sul tasto avvia in alto a destra. I parametri sotto permettono di modificare il punto in cui posizionare il file caricato.

Il file collegato sarà visualizzato con le dimensioni impostate nella voce :kbd:`Scaling` quindi, se dovesse essere troppo grande per il piano di lavoro, sarà sufficiente ridimensionarlo.
