.. include:: instruments_bar_images.txt

.. _barra_strumenti:

#####################
BARRA DEGLI STRUMENTI
#####################

.. _barra_strumenti_progetti:

********************
:guilabel:`PROGETTI`
********************
Cliccando sul tab :guilabel:`Project` appaiono le icone:

**Handle**
   * |instruments_bar_custom_file_new| **New**: crea un nuovo progetto;
   * |instruments_bar_custom_open| **Open**: accede al computer e permette la ricerca di un progetto esistente;
   * |instruments_bar_custom_save| **Save**: salva il progetto corrente [#fn1]_;
   * |instruments_bar_custom_saveas| **Save As**: salva il progetto corrente chiedendo di assegnare un nuovo nome.

.. [#fn1] Al primo salvataggio verrà richiesto  di salvare i parametri.

**Changes**
   * |instruments_bar_custom_undo| **Undo**: annulla l'ultima operazione eseguita;
   * |instruments_bar_custom_redo| **Redo**: ripristina l'ultima operazione annullata.


.. _barra_strumenti_macchina:

********************
:guilabel:`MACCHINA`
********************
**Red Options**
   * |instruments_bar_custom_machineRed| **Red**: esegue il comando anteprima rosso in macchina (se presente). La testa laser proietta il bordo della lavorazione per aiutare a centrare l'incisione sull'oggetto. Esistono 3 modalità di proiezione:

      1. **Red Edge**: proietta il contorno più esterno del disegno;
      2. **Red Square**: proietta un rettangolo detto *bounding box* che rappresenta l'ingombro massimo della lavorazione;
      3. **Red Circle**: proietta una circonferenza che rappresenta l'ingombro massimo della lavorazione.

**Marking**
   * |instruments_bar_custom_machineStart| **Start**: avvia la lavorazione;
   * |instruments_bar_custom_machineLight| **Light**: accende e spegne la luce all'interno della macchina;
   * |instruments_bar_custom_machineStop| **Stop**: ferma ogni attività, lavorazione, movimentazione, ecc...

**Status**
   * |instruments_bar_custom_machineDoorOpen| **Door Open**: apre la porta della macchina, attivando il relativo interblocco e mettendo il sistema in una condizione di sicurezza;
   * |instruments_bar_custom_machineDoorClose| **Door Close**: chiude la porta della macchina della macchine, permettendo la lavorazione.

   Stato di funzionalità dei principali componenti:

   * **Stato macchina**: indica se la comunicazione con il dispositivo elettrico che gestisce la macchina è attiva;
   * **Stato sorgente**: indica se la comunicazione con la sorgente laser è attiva;
   * **Scheda di scansione**: indica se la comunicazione con la scheda di scansione (componente che controlla la testa laser e muove gli specchi) è attiva.

   Il colore dei semafori indica lo stato di funzionamento:

   * *Verde*: tutto regolare;
   * *Giallo*: attivazione in corso;
   * *Rosso*: errori di comunicazione con il dispositivo.

**Velocità/Qualità**
   Regola con quanta precisione viene rappresentato il disegno da incidere. Maggiore è la qualità e minore sarà la velocità di esecuzione del disegno.

   Il laser è in grado di rappresentare solamente tratti rettilinei. Ogni curva viene ottenuta eseguendo un numero elevato di tratti rettilinei molto brevi (vettori). Il controllo *velocità/qualità* agisce direttamente sul calcolo di questi vettori. Più elevato è il numero di vettori (maggiore qualità) e maggiore sarà il tempo per processarli ed eseguirli.

   * |instruments_bar_custom_machineEdge| **Marcatura Bordi**: attiva la marcatura dei bordi dei disegni;
   * |instruments_bar_custom_machineFill| **Marcatura Riempimenti**: attiva la marcatura dei riempimenti dei disegni;
   * |instruments_bar_custom_machineOverlap| **Marcatura Sovrapposizioni**: attiva la gestione delle Sovrapposizioni.

   Quando due elementi grafici hanno una parte che si interseca si crea una sovrapposizione. Ai fini della lavorazione è possibile stabilire se:

   * eseguire il lavoro per entrambi gli elementi in modo completo; *oppure*
   * eseguire la sola parte visibile nel disegno, quindi quella più "alta" in termini di disposizione del layer.

   L'attivazione della marcatura delle sovrapposizioni porta ad una aggiunta di calcoli e quindi ad un incremento dei tempi di marcatura. Se non sono richieste passate multiple si suggerisce di ottimizzare il disegno in modo da evitare la presenza di parti che si intersecano.

   * |instruments_bar_custom_machineTestingTool| **Strumenti di test**: contiene un set di strumenti utili per eseguire regolazioni e messa a punto della macchina. Alcune funzionalità possono essere legate a plugin e, quindi, comparire solo se abilitate al momento dell'installazione.

**Componenti** 
   * |instruments_bar_rotate-right| :guilabel:`Turn table`: rotazione della tavola rotante (se presente):

      * :guilabel:`NORMAL`: permette la gestione della tavola rotante;
      * :guilabel:`SLOW`: permette la gestione più lenta della tavola rotante, per non sbilanciare eventuali pezzi posizionati;

   * |instruments_bar_weather-windy| :guilabel:`Change air flow A`: abilita il flusso d'aria per il bloccaggio del pezzo montato su mandrino rotante;
   * |instruments_bar_weather-windy| :guilabel:`Change air flow B`: abilita il flusso d'aria per il bloccaggio del pezzo montato su mandrino rotante.

   .. figure:: _static/SART3.png
      :width: 14 cm
      :align: center

.. _barra_strumenti_disegno:

*******************
:guilabel:`DISEGNO`
*******************
**Geometries**
==============

.. _fig_barraStrumenti_disegno:
.. figure:: _static/barraStrumenti_disegno.png
   :width: 18 cm
   :align: center

   Barra degli strumenti disegno

* |instruments_bar_custom_pin| **Default**: visualizza i comandi standard;
* |instruments_bar_custom_preferred| **Preferiti**: accede alla barra degli strumenti personalizzata.

Nella modalità **Preferiti** è possibile selezionare quali comandi mantenere visibili. Questa opzione limita il numero di comandi mantenendo i più usati, per ridurre la complessità della finestra.

Per attivare o disattivare i comandi:

* cliccare sul pulsante |instruments_bar_custom_add|;
* selezionare o deselezionare i comandi che si vogliono tenere o nascondere;
* confermare cliccando sul pulsante |instruments_bar_custom_saveSettings|.

.. _fig_barraStrumenti_disegnoCustom:
.. figure:: _static/barraStrumenti_disegnoCustom.png
   :width: 10 cm
   :align: center

   Barra degli strumenti disegno preferiti

.. _fig_barraStrumenti_disegnoCustomChange:
.. figure:: _static/barraStrumenti_disegnoCustomChange.png
   :width: 18 cm
   :align: center

   Barra degli strumenti disegno modifica preferiti

* |instruments_bar_custom_open| **Apri file**: permette di caricare un file grafico nell'area di lavoro. Possono essere caricati diversi tipi di file:

   * *File 3D*: stl, obj;
   * *Immagini vettoriali*: svg, svgz;
   * *Disegni da CAD*: dxf, dwg;
   * *Immagini*: png, jpg, jpeg, bmp, tiff;
   * *File per scavo 3d già stratificati* descritti da un file di testo txt.

* |instruments_bar_custom_drawLine| **Disegna linea**: permette di disegnare una linea nell'area di lavoro:

   * selezionare il comando e cliccare col mouse nel punto di origine della linea;
   * cliccare col mouse nel punto finale della linea;
   * premere :kbd:`Enter` per confermare.

* |instruments_bar_custom_drawSpiral| **Disegna spirale**: permette di disegnare una spirale nell'area di lavoro:

   * selezionare il comando e cliccare col mouse sul punto di origine della spirale;
   * impostare la grandezza della spirale e cliccare col mouse per confermare;
   * selezionare lo spazio tra le linee della spirale e cliccare col mouse per confermare;
   * premere :kbd:`Enter` per confermare.

* |instruments_bar_custom_drawSpline|: funziona come il comando |instruments_bar_custom_drawLine| ma permette di disegnare 2 tipi di linee:

   #. **Linea spezzata**:

      * selezionare il comando e cliccare col mouse nel punto di origine della linea;
      * cliccare con il mouse in tutte le posizioni dei punti che formano la linea;
      * premere :kbd:`Enter` per confermare.

   #. **Linea curva**:

      * selezionare il comando e cliccare col mouse nel punto di origine della linea;
      * cliccare col mouse nel successivo punto della linea e mantenendo il click muovere il mouse per correggere la curvatura;
      * premere :kbd:`Enter` per confermare.

* |instruments_bar_custom_drawThickedSpline|: funziona come il comando |instruments_bar_custom_drawSpline| ma la linea è più spessa (non può essere marcata con i bordi).

* |instruments_bar_custom_drawCircle| **Disegna cerchio**: permette di disegnare un cerchio nell'area di lavoro:

   * selezionare il comando e cliccare col mouse sul punto centrale del cerchio;
   * selezionare la lunghezza del raggio e cliccare col mouse per confermare;
   * premere :kbd:`Enter` per confermare.

* |instruments_bar_custom_drawEllipse| **Disegna ellisse**: permette di disegnare un'ellisse nell'area di lavoro:

   * selezionare il comando e cliccare col mouse sul punto centrale dell'ellisse;
   * selezionare la lunghezza del semiasse orizzontale e cliccare col mouse per confermare;
   * selezionare la lunghezza del semiasse verticale e cliccare col mouse per confermare;
   * premere :kbd:`Enter` per confermare.

* |instruments_bar_custom_drawRectangle| **Disegna rettangolo**: permette di disegnare un rettangolo nell'area di lavoro:

   * selezionare il comando e cliccare col mouse sul primo spigolo del rettangolo e cliccare col mouse per confermare;
   * selezionare la dimensione del rettangolo e cliccare col mouse per confermare;
   * premere :kbd:`Enter` per confermare.

* |instruments_bar_custom_drawText| **Disegna testo**: permette di aggiungere un testo nell'area di lavoro:

   * selezionare il comando e cliccare col mouse sul punto di inizio del testo;
   * scrivere il testo desiderato;
   * premere :kbd:`Enter` per confermare.

* |instruments_bar_custom_drawTextCircled| **Disegna testo circolare**: permette di aggiungere un testo circolare nell'area di lavoro:

   * selezionare il comando e cliccare col mouse sul punto centrale del cerchio;
   * selezionare la lunghezza del raggio e cliccare col mouse per confermare;
   * scrivere il testo desiderato;
   * premere :kbd:`Enter` per confermare.

* |instruments_bar_custom_drawTextOnPath| **Disegna testo su linea**: permette di aggiungere un testo su una linea nell'area di lavoro:

   * selezionare il comando e cliccare col mouse nel punto di origine della linea;
   * cliccare col mouse nel punto finale della linea e,tenendo premuto il tasto sinistro del mouse, selezionare la curvatura;
   * premere :kbd:`Enter` per confermare.
   * scrivere il testo desiderato;
   * premere :kbd:`Enter` per confermare.

* |instruments_bar_custom_draw1DBarcode| **Disegna un codice a barre**: permette di aggiungere un codice a barre nell'area di lavoro:

   * selezionare il comando e cliccare col mouse nel punto di origine del codice a barre;
   * premere :kbd:`Enter` per confermare.

* |instruments_bar_custom_draw2DBarcode| **Disegna una datamatrix**: permette di aggiungere una datamatrix nell'area di lavoro:

   * selezionare il comando e cliccare col mouse nel punto di origine della datamatrix;
   * premere :kbd:`Enter` per confermare.

* |instruments_bar_custom_draw3DShapes| **Disegna un solido 3D**: permette di disegnare un solido 3D nell'area di lavoro:

   I modelli 3D consentono di generare delle geometrie tridimensionali da utilizzare nelle lavorazioni.
   Ad ognuna di queste geometrie viene associata una mappatura UV della superficie che consente di rappresentare nel piano la lavorazione. Alcuni modelli tridimensionali compaiono più volte nell'elenco perché possono essere associate diversi modelli di mappatura UV. Questo argomento verrà esposto in dettaglio più avanti.

.. _fig_SLC3_draw_Custom3dShape:
.. figure:: _static/SLC3_draw_Custom3dShape.png
   :width: 14 cm
   :align: center

   Finestra disegno e parametrizzazione forma 3D.

I possibili modelli con i relativi parametri sono:

* :guilabel:`Ring`:

   * Diametro esterno [mm]
   * Diametro interno [mm]
   * Lunghezza [mm]

* :guilabel:`Hemi Cylinder`:

   * Diametro esterno [mm]
   * Lunghezza [mm]

* :guilabel:`Cylinder`:

   * Diametro esterno [mm]
   * Lunghezza [mm]

* :guilabel:`Sphere 1`:

   * Diametro esterno [mm]

* :guilabel:`Sphere 2`:

   * Diametro esterno [mm]

* :guilabel:`Plane`:

   * Larghezza [mm]
   * Altezza [mm]
   * Inclinazione [°]

.. _fig_SLC3_3dPreview:
.. figure:: _static/SLC3_3dPreview.png
   :width: 14 cm
   :align: center

   Anteprima della forma 3D con mappatura UV.

**Snaps**
=========
Gli **Snap** sono degli strumenti che forniscono una posizione precisa rispetto un oggetto o il piano di lavoro.

* |instruments_bar_custom_snapGrid| **Snap su griglia**: attiva e disattiva la funzione per agganciare gli oggetti agli incroci formati dalla griglia del piano di lavoro (passo 1mm);
* |instruments_bar_custom_snapMagnet| **Snap oggetto**: attiva e disattiva la funzione di aggancio sulle linee guida;
* |instruments_bar_custom_snapEndPoint| **Snap oggetto su punto finale**: attiva e disattiva la funzione di aggancio del cursore sul punto finale dell'oggetto;
* |instruments_bar_custom_snapMidPoint| **Snap oggetto su punto medio**: attiva o disattiva la funzione di aggancio sul punto medio dell'oggetto.

**Edit**
========
* |instruments_bar_custom_viewCamera2D|, |instruments_bar_custom_viewCamera3D|: consentono di passare dalla visualizzazione bidimensionale a quella tridimensionale del piano di lavoro. Utile quando si ha la necessità di eseguire delle lavorazioni 3D;
* |instruments_bar_custom_editCopy| **Copia**: copia gli elementi selezionati nel piano di lavoro;
* |instruments_bar_custom_editPaste| **Incolla**: incolla nel piano di lavoro gli elementi copiati;
* |instruments_bar_custom_editDelete| **Cancella**: cancella gli elementi selezionati.
* |instruments_bar_custom_alignCenterSelection| **Posiziona al centro**: posiziona al centro del piano di lavoro gli elementi selezionati. Se gli elementi non sono raggruppati verranno tutti accentrati uno sopra l'altro;
* |instruments_bar_custom_alignAlignment| **Allineamenti**: apre un sotto menù che permette di usare diverse funzionalità:

   * |instruments_bar_custom_alignAlignmentTop|: allinea gli oggetti in alto;
   * |instruments_bar_custom_alignAlignmentBottom|: allinea gli oggetti in basso;
   * |instruments_bar_custom_alignAlignmentRight|: allinea gli oggetti a destra;
   * |instruments_bar_custom_alignAlignmentLeft|: allinea gli oggetti a sinistra;
   * |instruments_bar_custom_alignAlignmentHorizontalCenter|: allinea gli oggetti al centro orizzontalmente;
   * |instruments_bar_custom_alignAlignmentVerticalCenter|: allinea gli oggetti al centro verticalmente;

* |instruments_bar_custom_editArray| **Crea serie di oggetti**: attraverso un sotto menù a comparsa, vengono decisi i parametri di copia:

   * :guilabel:`Array Matrix`: crea una matrice quadrata impostando il numero di righe e colonne e la distanza tra gli elementi che la compongono.

      * selezionare le impostazioni desiderate;
      * selezionare l'oggetto;
      * premere :kbd:`Array`;
      * selezionare il punto in cui porre il centro della matrice;
      * premere :kbd:`Enter` per creare la matrice.

   * :guilabel:`Array Radial`: crea una matrice rotonda con gli elementi disposti lungo la circonferenza. Si può impostare il numero di elementi da disporre e la spaziatura (in gradi).

      * selezionare le impostazioni desiderate;
      * selezionare l'oggetto;
      * premere :kbd:`Array`;
      * selezionare il punto in cui porre il centro della matrice;
      * premere :kbd:`Enter` per per creare la matrice.

Le operazioni di raggruppamento sono utili per muovere, ruotare e scalare oggetti mantenendo fisse le loro posizioni relative.

* |instruments_bar_custom_editItemsOrder| **Ordine di posizionamento**: apre il menu che consente di selezionare l'ordine di posizionamento degli oggetti:

   * |instruments_bar_custom_editLevelTop|: sposta l'elemento selezionato sopra tutti gli altri elementi;
   * |instruments_bar_custom_editLevelUp|: sposta l'elemento selezionato sopra un altro elemento cambiando così l'ordine di lavorazione;
   * |instruments_bar_custom_editLevelDown|: sposta l'elemento selezionato sotto un altro elemento cambiando così l'ordine di lavorazione;
   * |instruments_bar_custom_editLevelBottom|: sposta l'elemento selezionato sotto tutti gli altri elementi.

L'ordine di disegno dei vari elementi coincide con l'ordine delle lavorazioni e può essere importante per decidere quale lavorazione viene prima di altre oppure per ottimizzare i tempi di lavoro riducendo i salti più ampi.

* |instruments_bar_custom_editOffset| **Offset**: riduce o allarga le dimensioni dell'oggetto a partire dal suo contorno:

   * selezionare l'oggetto;
   * premere sulla *freccetta* del pulsante per inserire l'offset in mm;
   * digitare un offset positivo per estrudere l'oggetto, oppure un offset negativo per intruderlo;
   * premere :kbd:`Enter` per confermare.

* |instruments_bar_custom_editGroup| **Group**: raggruppa gli oggetti selezionati in un unica figura;
* |instruments_bar_custom_editUngroup| **Ungroup**: divide gli oggetti raggruppati nelle singole figure.

.. _barra_strumenti_gestione_anello:

******************
PROPRIETÀ GRAFICHE
******************
* **Windth [mm]**: larghezza file;
* **Height [mm]**: altezza file;
* **Block**: blocca le proporzioni grafiche del file;
* **Rotation [degs]**: gestione rotazioni;
* **Anchor point**: rappresenta la gestione delle coordinate "X" e "Y" di ogni singolo file grafico;

.. _fig_pro.graf:
.. figure:: _static/pro.graf.png
   :width: 14 cm
   :align: center

****************
GESTIONE ANELLO
****************
* **Ring Measure**: misura dell'anello da lavorare;
* **Length**: lunghezza dell'anello steso sul piano;
* **Inner marking first**: marcatura con inizio dalla parte interna;
* **Engraving tilt**; 
* **Vision tilt**;
* **Camera exposure**: percentuale esposizione luminosa;
* **Automatic positioning**; 
* **Use ring data sheet**; 
* **Import ring data**;
* |instruments_bar_database-search| **Show datasheet**: mostra foglio dati;
* |instruments_bar_ring| **Place ring**;
* |instruments_bar_counter| **Update counters**.

.. _barra_strumenti_accessori:

*********
ACCESSORI
*********
Sezione dedicata agli strumenti e funzionalità visualizzabili quando connessi alla macchina (se presenti).

**Mandrino**
   * :guilabel:`Homing`: azzeramento mandrino;
   * :guilabel:`Move Backward`: muove il mandrino in avanti del numero di gradi impostati in :guilabel:`Step`;
   * :guilabel:`Move Forward`: muove il mandrino indietro del numero di gradi impostati in :guilabel:`Step`.
   * :guilabel:`Step`: avanzamento mandrino per gradi;
   * :guilabel:`Release`: rilascia mandrino dalla coppia;
   * :guilabel:`Goto abs`: vai a grado assoluto impostato;
   * :guilabel:`Current position`: grado posizione attuale;
   * :guilabel:`Zero software`: permette di inserire uno "zero virtuale" o "origine" per facilitare le lavorazioni più complesse;
   * :guilabel:`Setup mandrel`.

**Setup Mandrino**
   .. _fig_SM:
   .. figure:: _static/SM.png
      :width: 14 cm
      :align: center

      Pagina :guilabel:`Setup Mandrino`

   La configurazione del mandrino è un'operazione fondamentale per usufruire delle funzionalità di quest'ultimo.

   Si trova nella :guilabel:`Barra strumenti` nella parte dedicata agli :guilabel:`Strumenti` e si compone di (:numref:`fig_SM`):

   * :guilabel:`Lato di posizionamento`: riferito al lato nel quale è stato fissato il mandrino (sinistra destra ecc...);
   * :guilabel:`Altezza`: altezza del centro rotante (h nella :numref:`fig_h`);

   .. _fig_h:
   .. figure:: _static/h.png
      :width: 14 cm
      :align: center

      Altezza del centro rotante

   * :guilabel:`Inclinazione predefinita`: inclinazione predefinita;
   * :guilabel:`Lunghezza braccio`: lunghezza totale del braccio (l nella :numref:`fig_l`);

   .. _fig_l:
   .. figure:: _static/l.png
      :height: 12 cm
      :width: 14 cm
      :align: center

      Lunghezza braccio

   * :kbd:`Posiziona mandrino`: l'interfaccia utente di impostazione del mandrino verrà nascosta e nell'area di lavoro verrà visualizzata una croce verde. Una volta posizionato sul riferimento mandrino, l'utente deve premere il pulsante :guilabel:`Enter` per confermare quella posizione e tornare alla finestra Configurazione mandrino;
   * :kbd:`Vai a posizione`: l'asse X e l'asse Y si muovono sulle posizioni impostate;
   * :kbd:`Posizionamento pinze`: il setup mandrino verrà nascosto e una croce verde sarà visualizzata nell'area di lavoro. Una volta posizionato sulle pinze, l'utente dovrà premere il pulsante :guilabel:`Return` per confermare quella posizione e tornare alla finestra :guilabel:`Mandrel Setup`.
