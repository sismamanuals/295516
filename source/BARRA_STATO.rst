.. _barra_di_stato:

##############
BARRA DI STATO
##############

La barra di stato si trova sulla parte inferiore della finestra dell'applicazione. Il suo scopo è quello di visualizzare informazioni relative allo stato di funzionamento del sistema.

.. _toolbar_general_footer:
.. figure:: _static/toolbar_general_footer.png
   :width: 14 cm
   :align: center

   :guilabel:`Barra di stato`

La sezione si suddivide in: 

* :kbd:`Waiting job`: indica graficamente, mediante una barra di avanzamento, lo stato della lavorazione. Nel caso dello start di una lavorazione la voce cambierà in **Marking Job** ed in **Error** nel caso di errore di quest'ultima. 
* :kbd:`Process Time`: indica il tempo trascorso dall'avvio del ciclo di lavorazione;
* :kbd:`Laser Time`: indica il tempo trascorso dall'inizio della lavorazione in corso;
* :kbd:`Messages`: indica gli eventi di allarme e i messaggi di errore. Cliccando sull'icona si accede alla finestra messaggi che comparirà sul lato sinistro.
