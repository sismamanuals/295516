.. include:: lateral_window_images.txt

.. _finestre_laterali:

#################
FINESTRE LATERALI
#################
La porzione laterale di destra (:numref:`fig_SLC3_lateral_window`) viene dedicato alla gestione delle seguenti operazioni:

* :guilabel:`Project Explorer`
* :guilabel:`Parameters` 
* :guilabel:`Counters` 
* :guilabel:`Graphic objects`
* :guilabel:`Axes`
* :guilabel:`DataSheet`

.. _fig_SLC3_lateral_window:
.. figure:: _static/SLC3_project_02.png
   :width: 8 cm
   :align: center

   SLC³ finestra laterale

.. _finestre_laterali_progetto:

****************************
:guilabel:`PROJECT EXPLORER`
****************************
È dedicata alla gestione complessiva dei progetti per l'utilizzo della macchina, i quali sono rinominabili e riordinabili a preferenza. Si suddivide in:

:guilabel:`Laser Job`
   Riguarda la gestione generale dei progetti, e al suo interno racchiude la sottogestione di tutti i :guilabel:`Working Plane` e :guilabel:`Marking Job` creati. 

   :guilabel:`Working Plane`
      È la sottofinestra di gestione di tutti i :guilabel:`Marking Job` creati al suo interno. Ogni :guilabel:`Working Plane` al suo interno può contenere "*n*" sottoprogetti distinti.

      :guilabel:`Marking Job`
         Rappresenta ogni singola lavorazione nello specifico. Al suo interno può contenere file grafici e specifici pre e post-steps.

.. NOTE::
   |notice| Nel caso di utilizzo di una macchina modello SART sarà abilitato il pulsante in :numref:`fig_SART2` che permette la gestione dei diversi piani di lavoro rispettivamente nei :guilabel:`Working Plane` e nei :guilabel:`Marking Job` per la parte A o B della tavola.

.. _fig_SART2:
.. figure:: _static/SART2.png
   :width: 14 cm
   :align: center

   Pulsante di gestione dei piani di lavoro

:guilabel:`Pre-step e Post-step`
   Selezionando il pulsante in :numref:`fig_Finestra_laterale_pre_poststep` è inoltre possibile abilitare i Pre e Post-steps per usufruire di maggiori funzionalità in fasi di inizio o fine di ogni singolo progetto.

   .. _fig_Finestra_laterale_pre_poststep:
   .. figure:: _static/Finestra_laterale_pre_poststep.png
      :width: 14 cm
      :align: center

      Pagina di abilitazione Pre e Post-steps

   Premere il pulsante |lateral_window_plus-circle-outline| per scegliere l'operazione desiderata tra la lista di categorie.

   .. _fig_Finestra_laterale_Categories:
   .. figure:: _static/Finestra_laterale_Categories.png
      :width: 14 cm
      :align: center

      Pagina di gestione categorie Pre e Post-steps

:guilabel:`General`
   Riguarda le funzionalità abilitate all'interno di ogni: :guilabel:`Laser Job`, :guilabel:`Working Plane` e :guilabel:`Marking Job`:

   * :guilabel:`Save variable`: salva le variabili;
   * :guilabel:`Pause Machine`: interrompe il funzionamento della macchina;
   * :guilabel:`Open door`: apre la porta della macchina laser;
   * :guilabel:`Air flow step`: permette la gestione dei flussi di aria o di gas.

:guilabel:`Axes` 
   * :guilabel:`Axes goto rel`: movimenta gli assi con coordinate relative rispetto alla posizione attuale;
   * :guilabel:`Axes goto abs`: movimenta gli assi con coordinate assolute e li posiziona alle quote desiderate.
   * :guilabel:`Homing`: azzera uno o più assi, in base alla selezione.

:guilabel:`Pattern Matching` 
   * :guilabel:`Positioning`: abilita il pattern matching (:numref:`barra_strumenti_pattern_matching`) all'interno di un pre-step o post-step generico. Permette di cercare modelli differenti per ogni steps nei quali è abilitato.

:guilabel:`Default`
   * :guilabel:`Lock piece`: blocco pezzo;
   * :guilabel:`Check gas Flow`: verifica quantità flusso gas;
   * :guilabel:`Unlock piece`: sbocco pezzo
   * :guilabel:`Close gas`: chiusura gas 
   * :guilabel:`Open gas`: apertura gas

:guilabel:`I/Os`
   * :guilabel:`Check input`: verifica stato ingresso digitale (varia in base alle configurazioni di ogni singola macchina);
   * :guilabel:`Wait for input`: attendi cambio di stato ingresso digitale (varia in base alle configurazioni di ogni singola macchina);
   * :guilabel:`Set output`: cambio stato uscita digitale (varia in base alle configurazioni di ogni singola macchina); 

.. NOTE::
   |notice| Le categorie presenti possono variare in base al modello di macchina in uso.

.. _finestre_laterali_parametri:

**********************
:guilabel:`PARAMETERS`
**********************
La finestra dedicata racchiude la gestione dei parametri della sorgente laser inserita nella macchina:

* |lateral_window_backup-restore| :kbd:`Back to origin`: torna ai precedenti parametri inseriti;
* |lateral_window_content-copy| :kbd:`Copy`: copia parametri;
* |lateral_window_playlist-plus| :kbd:`Expert mode`: apre la visualizzazione :kbd:`Expert mode` mostrando la gestione complessiva dei parametri laser (gestione ritardi laser);
* |lateral_window_custom_save| :kbd:`Save`: salva i parametri laser;
* |lateral_window_custom_saveas| :kbd:`Save as`: salva con nome i parametri laser;
* |lateral_window_FolderButton| :kbd:`Open layers file`: apre la libreria parametri dove è possibile visualizzare tutte le differenti parametrizzazioni laser salvate;
* :kbd:`SLC Process`: premendo la tendina compaiono diversi tipi di lavorazione. È necessario scegliere una della lavorazioni elencate per poter lavorare.

Una volta scelta la lavorazione desiderata compariranno a seguire i parametri laser di *default*. Selezionare con |lateral_window_FolderButton| la libreria dei parametri e le impostazioni più idonee per la lavorazione.

.. _finestre_laterali_variabili_e_contatori:

********************
:guilabel:`COUNTERS`
********************
In questa sezione si possono creare delle variabili alle quali vengono assegnati oggetti di tipo :guilabel:`text`, :guilabel:`text on curve` o :guilabel:`text circled`. Il loro valore viene rilevato da dati esterni (es. data/ora) o viene assegnato in modo incrementale per poterli poi utilizzare sul piano di lavoro (:numref:`fig_Counters_label`).

.. _fig_Counters_label:
.. figure:: _static/Counters_label.png
   :width: 14 cm
   :align: center

   Pagina :guilabel:`Counters`

Se la finestra appare vuota significa che non c'è alcun tipo di variabile o contatore attivo. Utilizzare:

* |lateral_window_plus| per creare una variabile o un contatore;
* |lateral_window_delete| per eliminare una variabile o un contatore.

Gestione Variabili
==================
Vediamo nel dettaglio le caratteristiche delle diverse modalità:

1. |lateral_window_alpha-a-box| :guilabel:`Char Counter`

.. _fig_Counters_char:
.. figure:: _static/Counters_char.png
   :width: 12 cm
   :align: center 

Assegna un valore variabile (in base ad un indice scelto dall'utente) ad un testo, in modo che cambi ad ogni marcatura. I parametri impostabili sono:

* :guilabel:`Index`: mostra il valore del contatore all'indice inserito;
* :guilabel:`Min Value`: è il valore di partenza del contatore (in questo caso possono essere inserite solo lettere);
* :guilabel:`Max Value`: è il valore massimo al quale il contatore può arrivare, se superato riparte dal valore :guilabel:`Min Value`;
* :guilabel:`Step`: indica di quanti passi avanza il contatore ad ogni ciclo.

2. |lateral_window_calendar-clock| :guilabel:`Ora e data`

.. _fig_Counters_date:
.. figure:: _static/Counters_date.png
   :width: 12 cm
   :align: center

Con questo contatore è possibile assegnare il valore di data e ora attuali. Cliccando sul pulsante |lateral_window_information| è possibile visualizzare tutti i valori e le diverse formattazioni disponibili.

3. |lateral_window_pound-box| :guilabel:`V3`

.. _fig_Counters_V3:
.. figure:: _static/Counters_V3.png
   :width: 12 cm
   :align: center

4. |lateral_window_numeric| :guilabel:`Incrementale`

.. _fig_Counters_Int:
.. figure:: _static/Counters_Int.png
   :width: 12 cm
   :align: center

Assegna un valore variabile (in base ad un indice scelto dall'utente) ad un testo, in modo che cambi ad ogni marcatura. I parametri impostabili sono:

* :guilabel:`Index`: mostra il valore del contatore all'indice inserito;
* :guilabel:`Min Value`: è il valore di partenza del contatore (in questo caso possono essere inserite solo numeri);
* :guilabel:`Max Value`: è il valore massimo al quale il contatore può arrivare, se superato riparte dal valore :guilabel:`Min Value`;
* :guilabel:`Step`: indica di quanti passi avanza il contatore ad ogni ciclo.

5. |lateral_window_counter| :guilabel:`V5`

.. _fig_Counters_V5:
.. figure:: _static/Counters_V5.png
   :width: 12 cm
   :align: center

6. |lateral_window_alphabetical| :guilabel:`Testo statico`

.. _fig_Counters_static:
.. figure:: _static/Counters_static.png
   :width: 12 cm
   :align: center

Inserisce una stringa riportabile su più testi (es. loghi) in modo da non doverla inserire più volte. Per l'inserimento ripetuto assegnare il contatore al testo sul piano di lavoro.

.. _finestre_laterali_oggetti_grafici:

***************
OGGETTI GRAFICI
***************
Contiene la visualizzazione di tutti i file grafici inseriti all'interno di ogni singolo progetto.

.. _finestre_laterali_assi:

****************
:guilabel:`AXES`
****************
.. CAUTION::
   |caution| RISCHIO DI DANNI A COSE E/O PERSONE

   I comandi di movimentazione non tengono conto di eventuali ingombri nella zona di lavoro (per esempio pezzi in lavorazione, attrezzature, ecc...). È compito dell'operatore verificare l'assenza di interferenze meccaniche tra le parti.

I sistemi laser più completi, oltre alla gestione della sorgente laser, offrono la possibilità di posizionare la testa galvanometrica.
Il range di movimento e il numero di assi di spostamento è legato alla tipologia di macchina e deve essere fatto rispettando le normative di sicurezza.

Il movimento viene gestito attraverso la pagina :guilabel:`Axes`, che si trova su una finestra laterale.

.. _fig_SLC3_toolWindow_AxesConsolleWithDisabledMandrel:
.. figure:: _static/SLC3_toolWindow_AxesConsolleWithDisabledMandrel.png
   :width: 10 cm
   :align: center

   Pagina :guilabel:`Axes`

La pagina :guilabel:`Axes` contiene il pannello di controllo degli assi cartesiani (X, Y, Z) e degli assi del mandrino che possono essere presenti nella macchina.
La pagina :guilabel:`Axes` si divide in quattro parti principali che aiutano a comprendere anche la suddivisione dei comandi:

* **Coordinate** (:numref:`finestre_laterali_assi_coordinate`);
* **Movimento** (:numref:`finestre_laterali_assi_movimento`);
* **Velocità** (:numref:`finestre_laterali_assi_velocità`);
* **Mandrino** (:numref:`finestre_laterali_assi_mandrino`) - solo se fisicamente presente.

.. _finestre_laterali_assi_coordinate:

Coordinate
==========
Le coordinate degli assi sono visualizzate nella parte superiore della finestra. 

La prima colonna contiene i valori della posizione istantanea. Il bordo della casella valore cambia il colore in arancione quando l'asse è in movimento. Queste informazioni non sono modificabili e sono fornite direttamente dal sistema di controllo assi. 

La seconda colonna contiene le quote per il posizionamento. I valori possono essere inseriti manualmente dall'operatore oppure si possono acquisire i valori istantanei facendo un doppio click sul campo stesso. Se la cella dove si inserisce il valore è disabilitata, significa che l'asse non è disponibile (in caso di mancato azzeramento dell'asse oppure di disabilitazione dell'asse).

I due pulsanti presenti:

* :kbd:`GO`: avvia il posizionamento verso le quote programmate. Se i dispositivi di protezione sono in posizione ed attivi la pressione del pulsante inizia il posizionamento. Se i dispositivi di protezione non sono attivi (per esempio porte aperte) è necessario mantenere la pressione del pulsante finché gli assi non raggiungono la quota impostata. Se l'azzeramento degli assi non è stato completato il tasto rimane disabilitato;
* |lateral_window_custom_axesAcquire|: legge la posizione attuale e la scrive sulle quote di posizionamento.

.. _finestre_laterali_assi_movimento:

Movimento
=========
Nella parte di movimento ci sono i comandi di **JOG**, sono dei pulsanti che consentono la movimentazione manuale di un'asse senza una quota di arrivo precisa. Anche in questo caso il movimento è possibile solo con i dispositivi di protezione in posizione ed attivi oppure, in casi particolari descritti nel manuale della macchina, sfruttando un comando ad azione mantenuta controllato dall'operatore.

.. CAUTION::
   |caution| RISCHIO DI DANNI A COSE E/O PERSONE
   
   I comandi di **JOG** possono essere eseguiti anche con assi non azzerati. In questa situazione le coordinate che vengono proposte possono non essere attendibili. L'operazione viene comunque concessa per consentire eventuali disimpegni da parte dell'automazione, ma è compito dell'operatore verificare l'assenza di interferenze meccaniche tra le parti.

Gli altri comandi presenti in questa sezione sono:

* |lateral_window_custom_axesKeyboard| **Tastiera**: abilita il controllo del **JOG** direttamente dalla tastiera dell'applicazione. Per ragioni di sicurezza il comando viene immediatamente disabilitato qualora l'utente esegua un click in qualunque punto oppure se la finestra principale perde il focus;
* |lateral_window_custom_axesHoming| **Homing**: esegue l'azzeramento degli assi. Il comando può essere avviato solamente quando i dispositivi di protezione sono in posizione ed attivi. Il comando di azzeramento si illumina per indicare all'operatore che è necessaria questa operazione e vengono impediti i comandi di posizionamento automatici;
* |lateral_window_custom_axesResetAlarms| **Reset allarmi**: azzera la segnalazione di allarmi in caso di loro presenza;
* |lateral_window_custom_machineStop| **Stop**: arresta l'esecuzione dei comandi in corso.

.. _finestre_laterali_assi_velocità:

Velocità
========
La velocità con cui si muovono gli assi può essere personalizzata secondo diverse esigenze (ad esempio sicurezza, precisione nei movimenti, ecc...). Nella sezione velocità è possibile definirne il valore in percentuale, rispetto un valore massimo del 100% definito.

Oltre al campo testuale dove viene inserito il valore sono presenti 3 pulsanti per la selezione rapida di alcuni valori comuni (0.2%, 2%, 25%), utili per eseguire delle centrature manuali.

Il valore 25% è il massimo raggiungibile con ripari aperti e si reputa che tale velocità sia sufficiente a non arrecare pericolo. Questo movimento a ripari aperti deve essere supportato da altri dispositivi di protezione (ad esempio comandi ad azione mantenuta, descritti nel manuale d'uso).

.. _finestre_laterali_assi_mandrino:

Mandrino
========

.. _fig_SLC3_toolWindow_AxesConsolle:
.. figure:: _static/SLC3_toolWindow_AxesConsolle.png
   :width: 10 cm
   :align: center

   Pagina :guilabel:`Axes` con mandrino attivo

La versione più completa di mandrino comprende due assi di rotazione chiamati:

* pulsanti :guilabel:`R` **Roll**: rotazione lungo l'asse X;
* pulsanti :guilabel:`T` **Tilt** rotazione lungo l'asse Y.

L'accessorio mandrino è dotato di una bascula 0-90°. Può essere rimosso in qualsiasi momento dalla macchina. Il pulsante presente nel software permette di disabilitarlo in modo agevole.
