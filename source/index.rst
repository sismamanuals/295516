.. PRISMA documentation master file, created by
   sphinx-quickstart on Tue Jan 7 13:21:25 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

#####################
Manuale Software SLC3
#####################

.. toctree::
   :maxdepth: 3
   :numbered:

   pittogrammi
   installazione
   INTERFACCIA_GRAFICA
   barra_del_titolo
   BARRA_STRUMENTI
   BARRA_STATO
   AREA_DI_LAVORO
   FINESTRE_LATERALI
   pattern_matching
   lavorazioni_su_anello
   sistema_di_visione
   impostazioni_DB_commesse
   Lite/interfaccia
   note
