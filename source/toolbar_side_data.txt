.. |toolbar_general_apri| image:: _static/toolbar_general_apri.png
   :height: 1.0 cm
   :align: middle

.. |toolbar_general_anteprima| image:: _static/toolbar_general_anteprima.png
   :height: 1.0 cm
   :align: middle

.. |toolbar_general_impostazioni| image:: _static/toolbar_general_impostazioni.png
   :height: 1.0 cm
   :align: middle

.. |toolbar_general_nuovo| image:: _static/toolbar_general_nuovo.png
   :height: 1.0 cm
   :align: middle

.. |toolbar_general_redo| image:: _static/toolbar_general_redo.png
   :height: 1.0 cm
   :align: middle

.. |toolbar_general_salva| image:: _static/toolbar_general_salva.png
   :height: 1.0 cm
   :align: middle

.. |toolbar_general_salva_nome| image:: _static/toolbar_general_salva_nome.png
   :height: 1.0 cm
   :align: middle

.. |toolbar_general_stop| image:: _static/toolbar_general_stop.png
   :height: 1.0 cm
   :align: middle

.. |toolbar_general_undo| image:: _static/toolbar_general_undo.png
   :height: 1.0 cm
   :align: middle

.. |toolbar_general_info| image:: _static/toolbar_general_info.png
   :height: 1.0 cm
   :align: middle

.. |toolbar_progetti_apri| image:: _static/toolbar_progetti_apri.png
   :height: 1.0 cm
   :align: middle

.. |toolbar_progetti_nuovo| image:: _static/toolbar_progetti_nuovo.png
   :height: 1.0 cm
   :align: middle

.. |toolbar_progetti_redo| image:: _static/toolbar_progetti_redo.png
   :height: 1.0 cm
   :align: middle

.. |toolbar_progetti_salva| image:: _static/toolbar_progetti_salva.png
   :height: 1.0 cm
   :align: middle

.. |toolbar_progetti_salva_nome| image:: _static/toolbar_progetti_salva_nome.png
   :height: 1.0 cm
   :align: middle

.. |toolbar_progetti_undo| image:: _static/toolbar_progetti_undo.png
   :height: 1.0 cm
   :align: middle

.. |toolbar_draw_apri| image:: _static/toolbar_draw_apri.png
   :height: 1.0 cm
   :align: middle

.. |toolbar_draw_line| image:: _static/toolbar_draw_linea.png
   :height: 1.0 cm
   :align: middle

.. |toolbar_draw_spiral| image:: _static/toolbar_draw_spirale.png
   :height: 1.0 cm
   :align: middle
