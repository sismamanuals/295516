.. include:: toolbar_vision_system.txt

.. _sistema_di_visione:

##################
SISTEMA DI VISIONE
##################
****************
BARRA OPERAZIONI
****************
* |toolbar_Vision_System_Live| :kbd:`Live`: attiva in presa diretta la telecamera al centro del piano di lavoro;
* |toolbar_Vision_System_Scan| :kbd:`Scan`: avvia la scansione del piano di lavoro. Per avviare una scansione, con un live attivo, bisogna prima premere il pulsante |toolbar_Vision_System_Stop| e poi il pulsante |toolbar_vision_system_camera|. Il Live può essere avviato se vi è una scansione già fatta;
* |toolbar_Vision_System_Stop| :kbd:`Stop`: blocca tutte le operazioni che la telecamera sta eseguendo;
* |toolbar_vision_system_camera| :kbd:`Clean`: pulisce il piano di lavoro eliminando tutte le immagini acquisite e il live;
* :kbd:`Exposition`: modifica il tempo di esposizione per rendere l'immagine più chiara o scura;
* :kbd:`Gain`: modifica il guadagno dell'immagine riducendo i tempi per l'acquisizione dell'area di lavoro;
* :kbd:`Acquisition area`: parametro di impostazione per l'acquisizione anello su mandrino. Avviando la scansione il mandrino ruoterà, permettendo di ottenere una scansione di tutta la superficie dell'anello, visualizzando lo sviluppo sul piano di lavoro;
* |toolbar_vision_system_custom_save| :kbd:`Save Area`: salva l'immagine acquisita sul piano di lavoro;
* |toolbar_Vision_System_Live| :kbd:`Live Area`: permette l'immagine live calibrata per un massimo di 40x40 mm.
* |toolbar_vision_system_Mandrel_calibration| :kbd:`Mandrel Calibration`: E' uno strumento di verifica e di gestione del mandrino. Permette di ridefinire e modificare, visivamente grazie all'ausilio della camera di visione, i parametri e le quote mandrino. 

************
CALIBRAZIONE
************
Questa procedura, attivabile solo se la telecamera è collegata alla porta USB, è necessaria per creare una corrispondenza pixel/millimetro, in modo da poter ricostruire senza distorsione l'area di lavoro inquadrata. Per effettuare la calibrazione si utilizza l'apposito target corrispondente alla focale (es. F100, F160, F254 ecc...). In seguito ad un cambio focale o movimento della telecamera, è necessario calibrare nuovamente il sistema.

L'interfaccia di calibrazione si trova nelle :guilabel:`Impostazioni` (vedi anche :numref:`impostazioni_generali`) di SLC³ alla sezione :guilabel:`Camera`, e contiene quattro sezioni evidenziate nella :numref:`fig_Calibration_labels`.

1. :guilabel:`Settings`: gestisce la selezione della focale, tempo d'esposizione della telecamera e i valori di movimento degli specchi;
2. :guilabel:`Positions matrix`: include l'area di lavoro suddivisa in celle; l'operatore, selezionandone una, comanda il movimento degli specchi in modo da visualizzarla; 
3. :guilabel:`Live & Focus`: è la finestra di visualizzazione detta al punto precedente, oltre a presentare il valore del fuoco.
4. :guilabel:`Calibration Pipeline`: contiene tutte le fasi e i dati di risposta.

.. _fig_Calibration_labels:
.. figure:: _static/Calibration_labels.png
   :width: 19 cm
   :align: center

   Pagina :guilabel:`Calibrazioni`

Procedura di calibrazione
=========================
Nella sezione :guilabel:`Settings` si può selezionare la focale e muovere gli specchi della testa di scansione grazie ai parametri:

* :guilabel:`Scan Offset X`;
* :guilabel:`Scan Offset Y`;
* :guilabel:`Live Offset X`;
* :guilabel:`Live Offset Y`;
* :guilabel:`Edge bit size`.

Per la calibrazione:

1. premere il pulsante |toolbar_vision_system_Mandrel_calibration| per orientare gli specchi verso il centro dell'area di lavoro;
2. posizionare il target sul piano di lavoro;
3. centrare il target riferendosi alla croce e controllando l'inclusione del target nelle varie posizioni di riconoscimento: il cerchio più in basso deve risultare al centro della singola cella. Le dimensioni di quest'ultima si modificano con i parametri descritti di :guilabel:`Offset` e di :guilabel:`Edge bit size`.

La finestra :guilabel:`Live & Focus` permette di visualizzare nel dettaglio l'anteprima della porzione di target che sarà scansionata dal sistema.

L'immagine in :numref:`fig_Good`, ad esempio, è idonea: il cerchio più in basso si trova al centro dell'area e il modulo 5x5 è contenuto nella finestra. Durante l'acquisizione una griglia verde riconosce e si sovrappone all'immagine.

.. _fig_Good:
.. figure:: _static/Good.png
   :width: 14 cm
   :align: center

   Esempio di immagine idonea

Se le componenti del modulo 5x5 non sono completamente inclusi e inquadrati nella finestra di acquisizione, come in :numref:`fig_Bad`, allora la griglia non apparirà o sarà incompleta.

.. _fig_Bad:
.. figure:: _static/Bad.png
   :width: 14 cm
   :align: center

   Esempio di immagine non idonea

L'operatore può variare i valori del tempo d'esposizione in modo da ottenere un'immagine nitida, nè troppo scura nè bruciata, con angoli della scacchiera nitidi.

Verificata la correttezza e qualità di tutte queste componenti la procedura essere avviata con il pulsante |toolbar_Vision_System_CalibrationButton|. L'avanzamento della procedura è mostrato nella barra laterale. 
Il software effettuerà:

1. scansione del target;
2. riconoscimento degli angoli di ogni immagine;
3. elaborazione dei calcoli di calibrazione;
4. correzione della deformazione di ogni immagine, confrontandola con le vicine.

Esito calibrazione
==================
Il buon esito dell'operazione avviene quando:

* le celle nell'area :guilabel:`Positions matrix` sono di colore verde;
* l'errore di correzione risulta inferiore a 0.8 pixels.

L'esito della procedura può essere positivo anche se compaiono delle celle rosse in zona periferica. Questo perché non è stato possibile ricostruire l'immagine contenuta in esse. Se il numero di celle rosse supera il 20% la procedura termina segnalando l'errore e non permettendo il salvataggio.

Se l'errore di correzione risulta superiore a 1.2 pixel è necessario cambiare il tempo d'esposizione o aggiustare il fuoco della telecamera.

.. TIP::
   |tip| Per calibrare utilizzando immagini scansionate precedentemente premere il pulsante |toolbar_vision_system_FolderButton| per caricarle dall'apposita cartella.

Allineamento del Laser con l'anteprima della telecamera
-------------------------------------------------------
SLC³ permette un perfetto allineamento fra laser e l'anteprima della telecamera gestendo i valori :guilabel:`Live Offset X` e :guilabel:`Live Offset Y`.

Per verificare l'allineamento marcare una croce e visualizzarla in tempo reale. L'immagine in :numref:`fig_errore_live_offset` mostra un esempio di disallineamento.

.. _fig_errore_live_offset:
.. figure:: _static/errore_live_offset.png
   :width: 14 cm
   :align: center

   Esempio di disallineamento della telecamera

Per correggere l'allineamento basta agire sui valori di offset, come evidenziato in :numref:`fig_live_offsets_edit`. Tramite doppio click all'immagine a destra si può verificare l'esito e bontà dell'operazione.

Si consiglia di ripetere l'operazione di allineamento dopo ogni modifica al sistema.

.. _fig_live_offsets_edit:
.. figure:: _static/live_offsets_edit.png
   :width: 14 cm
   :align: center

   Modifica :guilabel:`Live Offsets`

*************
AUTOMAPPATURA
*************
Questa procedura è per l'allineamento del sistema laser con la telecamera coassiale e segue, rigorosamente, la calibrazione. È attiva solo se la telecamera è collegata alla porta USB.

La sua interfaccia si trova nelle :guilabel:`Impostazioni` di SLC³ alla sezione :guilabel:`Camera` dopo la :guilabel:`Calibrazione` ed è composta da quattro sezioni:

1. :guilabel:`Commands`: sono le funzioni che permettono di eseguire la procedura:

   * |toolbar_vision_system_custom_machineRed| proiezione del *rosso*;
   * |toolbar_vision_system_custom_StartMarking| avvio marcatura target (cerchi);
   * |toolbar_vision_system_camera| acquisizione e analisi delle immagini;
   * |toolbar_vision_system_Stop| stop;
   * |toolbar_vision_system_custom_save| salvataggio;

2. :guilabel:`Settings`: permette di gestire il tempo di esposizione della telecamera e i parametri di marcatura;
3. :guilabel:`Images matrix`: contiene tutte le celle in cui viene suddivisa l'area di lavoro scansionata, premendo su di una singola cella si vede nel dettaglio l'immagine selezionata;
4. :guilabel:`Selected image`: mostra il dettaglio della singola cella selezionata.

.. _fig_AM_interface_labels:
.. figure:: _static/AM_interface_labels.png
   :width: 14 cm
   :align: center

   Pagina per :guilabel:`Automappatura`

Procedura di automappatura
==========================
Dopo aver gestito a piacere (è possibile mantenere i valori già presenti) i parametri in :guilabel:`Settings`:

1. proiettare l'ingombro del target di calibrazione su una piastra premendo il pulsante |toolbar_vision_system_custom_machineRed|, in modo da posizionarsi correttamente;
2. avviare la marcatura premendo il pulsante |toolbar_vision_system_custom_StartMarking|;
3. terminata l'operazione di marcatura iniziare l'acquisizione con la telecamera e la calibrazione vera e propria premendo il pulsante |toolbar_vision_system_camera|.

.. NOTE::
   |notice| Un risultato positivo è segnalato dalla bordatura verde delle celle nell'area :guilabel:`Images matrix` e dal riconoscimento corretto delle tre circonferenze, come in :numref:`fig_AM_circle`. Per il dettaglio selezionare un *target*, che verrà visualizzato nella zona :guilabel:`Selected image`.

.. _fig_AM_circle:
.. figure:: _static/circle.png
   :width: 14 cm
   :align: center

   Esempio di esito positivo

Se il risultato è positivo per ogni ogni cella, sarà possibile salvare l'esito.

Procedura manuale
=================
Se l'operazione non ha esito positivo la cella apparirà con bordo **giallo**. La causa è il mancato riconoscimento delle circonferenze per via di possibili occlusioni, o un disturbo dovuto alla scarsa o eccessiva luminosità.

In alcuni casi è risolvibile con la modifica del tempo d'esposizione. In :numref:`fig_AM_Problem` un esempio: una cella risulta difettosa e il dettaglio riporta il messaggio di stato :guilabel:`Fail`.

.. _fig_AM_Problem:
.. figure:: _static/AM_Problem.png
   :width: 14 cm
   :align: center

   Esempio di cella difettosa

In questo caso bisogna attivare la **Procedura manuale**:

* cliccare due volte sull'immagine difettosa;
* cliccare nel punto in cui definiamo il centro delle circonferenze (:numref:`fig_AM_Manual`).

.. _fig_AM_Manual:
.. figure:: _static/AM_Manual.png
   :width: 14 cm
   :align: center

   Esempio procedura manuale - 1

Il riconoscimento automatico, inefficace, sarà rimpiazzato dal comando dell'operatore e il bordo della cella apparirà verde (:numref:`fig_AM_Manual2`). A questo punto, abilitandosi il tasto, si potrà salvare.

.. _fig_AM_Manual2:
.. figure:: _static/AM_Manual2.png
   :width: 14 cm
   :align: center

   Esempio procedura manuale - 2

Calibrazione mandrino
=====================
Premendo il pulsante :kbd:`Calibrazione mandrino` vengono abilitate le seguenti funzioni di correzione, con il quale l'utente può perfezionare i precedenti parametri utilizzando il sistema di visione e il **target di calibrazione del mandrino** (un cilindro con diametro pari a 10.0 mm e altezza pari a 12.00 mm).
Tale comando è disponibile nella finestra :guilabel:`Sistema di visione`.

Nella parte destra della finestra l'utente può:

* impostare il tempo di esposizione della telecamera;
* impostare i parametri del target di calibrazione del mandrino (diametro e altezza);
* visualizzare gli scostamenti dell'offset impostati durante la procedura.

L'intera operazione di correzione è divisa in tre step:

1. **Calibrazione orizzontale**: il mandrino si inclinerà a 0° e l'utente può trascinare la linea verticale verde sulla base del cilindro target. Per confermare premere sul pulsante :kbd:`Conferma` e procedere al passaggio successivo premendo il pulsante :kbd:`Calibrazione verticale`.

.. _fig_CM1:
.. figure:: _static/CM1.png
   :width: 14 cm
   :align: center

2. **Calibrazione verticale**: il mandrino si inclinerà a 90° e un cerchio verrà visualizzato sull'immagine live. Il cerchio deve sovrapporsi perfettamente alla circonferenza del cilindro di destinazione. Per fare ciò l'utente può trascinare e rilasciare il cerchio. Per confermare fare clic sul pulsante "Conferma" e procedere al passaggio successivo premendo il pulsante :kbd:`Salva e controlla`.

.. _fig_CM2:
.. figure:: _static/CM2.png
   :width: 14 cm
   :align: center

3. **Verifica**: in questa sezione il mandrino si inclinerà di 90° e ruoterà di 180°.Se il risultato non è buono (ad es. Il cerchio non corrisponde alla base del cilindro), controllare i parametri precedenti nell'interfaccia utente di configurazione del mandrino.

.. _fig_CM3:
.. figure:: _static/CM3.png
   :width: 14 cm
   :align: center

.. TIP::
   |tip| Questa operazione è consigliata ad ogni avvio della macchina, dopo l'operazione di homing.
