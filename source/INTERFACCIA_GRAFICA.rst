.. Definizione delle immagini inline

.. |custom_saveas_interfaccia_grafica| image:: _static/icons/custom_saveas.svg
   :height: 1.0 cm
   :align: middle

.. |custom_open_interfaccia_grafica| image:: _static/icons/custom_open.svg
   :height: 1.0 cm
   :align: middle

.. _interfaccia_grafica_SLC3:

########################
INTERFACCIA GRAFICA SLC³
########################

.. _primo_avvio:

***********
PRIMO AVVIO
***********
Al termine della procedura di installazione viene generata sul desktop un'icona per l'avvio di SLC³. Avviando SLC³ compare la finestra in :numref:`fig_SLC3_splash`, attiva fino al termine del caricamento.

.. _fig_SLC3_splash:
.. figure:: _static/SLC3_splash.png
   :width: 10 cm
   :align: center

   Finestra di caricamento SLC³

.. NOTE::
   |notice| Al centro della finestra di caricamento è presente il codice con la versione dell'applicazione. La versione è un riferimento temporale ed è utile per capire quali funzionalità sono disponibili.

Al termine del caricamento si presenterà la finestra di Login (:numref:`fig_SLC3_login`). I valori di default al primo avvio sono:

* nome = **user**
* password = **pass**

Inserire questi valori e confermare con :kbd:`Ok`.

.. _fig_SLC3_login:
.. figure:: _static/SLC3_login.png
   :width: 10 cm
   :align: center

   Finestra di login SLC³

Verrà visualizzata l'interfaccia principale, inizialmente vuota.

.. _fig_SLC3_firstRun:
.. figure:: _static/SLC3_firstRun.png
   :width: 16 cm
   :align: center

   SLC³ primo avvio

.. _creare_un_progetto:

******************
CREARE UN PROGETTO
******************
Tutti i lavori vengono organizzati a progetti e contengono tutte le informazioni relative ai disegni da incidere, alle parametrizzazioni laser e alla gestione delle automazioni connesse alla macchina.

Esempio di creazione di un progetto
===================================
Per creare un progetto (:numref:`fig_SLC3_project_01`):

#. dal menù :guilabel:`Progetto`, cliccando sul pulsante :kbd:`Nuovo`, si inizia la creazione di un nuovo progetto;
#. compare subito una griglia rappresentante l'area di lavoro della macchina;
#. dal menù :guilabel:`Disegno` si seleziona l'icona :kbd:`Testo`;
#. cliccare sull'area di lavoro per iniziare la creazione del testo;
#. digitare il contenuto del testo e confermare con :kbd:`Invio` (nell'esempio viene creata la scritta "Sisma").

.. _fig_SLC3_project_01:
.. figure:: _static/SLC3_project_01.png
   :width: 14 cm
   :align: center

   SLC³ primo progetto

Il progetto, per considerarsi completo, deve contenere una parametrizzazione laser. A lato dell'area di lavoro è presente la scheda :guilabel:`Parametri`, necessaria per definire una configurazione laser. Ad un nuovo progetto viene proposta una parametrizzazione di marcatura di default. Per associarla al progetto:

* salvarla con un nuovo nome |custom_saveas_interfaccia_grafica|; *oppure*
* caricare una configurazione sfogliando l'archivio dei file |custom_open_interfaccia_grafica|.

.. _fig_SLC3_project_02:
.. figure:: _static/SLC3_project_02.png
   :width: 10 cm
   :align: center

   SLC³ parametri laser

Al termine delle operazioni il progetto è pronto per essere salvato e si può procedere con l'incisione.

.. TIP::
   |tip|  Un progetto per essere salvato deve necessariamente contenere una parametrizzazione laser valida per cui è necessario salvare con la configurazione proposta oppure caricarne una esistente.

.. _interfaccia_grafica:

*******************
INTERFACCIA GRAFICA
*******************
L'ambiente di progettazione di SLC³ si compone di un'interfaccia grafica composta da:

* :guilabel:`Barra del titolo` (:numref:`barra_del_titolo`)
* :guilabel:`Barra degli strumenti` (:numref:`barra_strumenti`)
* :guilabel:`Area di lavoro` (:numref:`area_di_lavoro`)
* :guilabel:`Finestra strumenti laterali` (:numref:`finestre_laterali`)
* :guilabel:`Barra di stato` (:numref:`barra_di_stato`)
* :guilabel:`Finestra messaggi`

.. _fig_SLC3_interface_01:
.. figure:: _static/SLC3_interface_01.png
   :width: 14 cm
   :align: center

   SLC³ composizione interfaccia progettazione
